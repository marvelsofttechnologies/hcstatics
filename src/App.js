import React, { useState } from 'react';
import BodyWrapper from './Components/BodyWrapper';
import Header from './Components/Header';
import Footer from './Components/Footer';

function App(){

  const [isLight, setTheme] = useState (false);

  const changeTheme = () => {
      setTheme(!isLight)
  }
    return(
      <body className={isLight ? "light-mode" : ""} >
        <Header switch={changeTheme}/>
        <BodyWrapper />
        <Footer />
      </body>
    );
}

export default App;