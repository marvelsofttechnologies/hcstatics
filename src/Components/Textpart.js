import React from 'react';

const Textpart = () =>{
    return (
      <div className="text-part">
        <div className="text-wrapper">
          <h1 className="const">Under Construction!</h1>
          <p className="sub">
            Our website is currently undergoing scheduled constructional
            activities. <br /> We would launch as soon as we finally set up.{" "}
            <br /> Thank you for your patience
          </p>
          <div className="inputbtn">
            <input
              type="email"
              name="mail"
              className="email"
              placeholder="Enter your email"
            />
            <button className="butn">Send a Message</button>
          </div>
          <p className="sub">
            Sign up now to get early notification of our launch date!
          </p>
          <div className="imag">
            <img src="/Assets/harmoo.png" alt="tolg" />
          </div>
        </div>
      </div>
    );
}

export default Textpart;