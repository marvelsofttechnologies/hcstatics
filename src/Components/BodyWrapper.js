import React, { useState } from 'react';
import Textpart from './Textpart';


const BodyWrapper = () =>{

    return(
        <div className="body-wrapper">
            <div className="imgpart"></div>
            <Textpart />
        </div>
    );
}

export default BodyWrapper;