import React from 'react';

const Footer = () => {
    return (
      <footer>
        <div className="socials">
          <p className="stay">Stay in touch:</p>
          <a href="#">
            <i className="fab fa-facebook-f" />
          </a>
          <a href="#">
            <i className="fab fa-twitter" />
          </a>
          <a href="#">
            <i className="fab fa-instagram" />
          </a>
          <a href="#">
            <i className="fab fa-youtube" />
          </a>
        </div>
      </footer>
    );
}

export default Footer;