import React, { useState } from 'react';

const Header = (props) => {

    const [isToggle, setToggle] = useState(false);

    const changeIcon = () => {
        setToggle(!isToggle)
    }

    const changeSwitch = () => {
        changeIcon();
        props.switch();
    }

    return(
        <div>
            <nav>
                <a href="/" className="logo">
                </a>
                <div className="theme" onClick={changeSwitch}>
                    <i className={isToggle ? "far fa-lightbulb" : "far fa-moon"}></i>
                </div>
            </nav>
        </div>
    );
}

export default Header;